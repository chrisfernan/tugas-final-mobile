import 'package:flutter/material.dart';
import 'package:purnamastore/purnamaservice.dart';
import 'package:purnamastore/akunml.dart';
import 'package:purnamastore/item.dart';
import 'package:purnamastore/akunuser.dart';
import 'package:purnamastore/pengaturan.dart';
import 'package:purnamastore/about.dart';
import 'package:purnamastore/ui/uipenjualan.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Icon customIcon = Icon(Icons.search);
  Widget customSearchBar = Center(child: Text("Main Menu"));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: customSearchBar,
        actions: <Widget>[
          IconButton(
            icon: customIcon,
            onPressed: () {
              setState(() {
                if (this.customIcon.icon == Icons.search) {
                  this.customIcon = Icon(Icons.cancel);
                  this.customSearchBar = TextField(
                    textInputAction: TextInputAction.go,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Search",
                    ),
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 16.0,
                    ),
                  );
                } else {
                  this.customIcon = Icon(Icons.search);
                  this.customSearchBar = Center(child: Text("Main Menu"));
                }
              });
            },
          ),
          IconButton(icon: Icon(Icons.notifications_active), onPressed: () {}),
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("Christopher Fernando Cias"),
              accountEmail: new Text("christfcias@gmail.com"),
              currentAccountPicture: new GestureDetector(
                onTap: () {},
                child: new CircleAvatar(
                  backgroundImage: new NetworkImage(
                      'http://3.bp.blogspot.com/-7z9CVVSsalw/XhKj2Zn5R8I/AAAAAAAAF1Y/I6_VGSW0VnIzQCMpU3_fqu_oEMGzU6SrwCK4BGAYYCw/s1600/x.jpg'),
                ),
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/appimages/x.jpg'),
                    fit: BoxFit.scaleDown),
              ),
            ),
            new ListTile(
              title: new Text("Profile"),
              trailing: new Icon(Icons.account_box),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return AccountPage();
                }));
              },
            ),
            new ListTile(
              title: new Text("Pengaturan"),
              trailing: new Icon(Icons.settings_applications_rounded),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return SetPage();
                }));
              },
            ),
            new ListTile(
              title: new Text("About us"),
              trailing: new Icon(Icons.east_sharp),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return AboutPage();
                }));
              },
            ),
          ],
        ),
      ),
      body: new ListView(
        children: <Widget>[
          Image.asset('assets/appimages/x.jpg'),
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(
                      color: Theme.of(context).dividerColor,
                      style: BorderStyle.solid),
                  bottom: BorderSide(
                      color: Theme.of(context).dividerColor,
                      style: BorderStyle.solid)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return PurnamaMLJokiPage();
                    }));
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.handyman_rounded,
                        color: Colors.black,
                        size: 50,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: Text(
                          "Purnama Service",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return StarterAkunPage();
                    }));
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.person,
                        color: Colors.red,
                        size: 50,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: Text(
                          "Jual Akun",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(
                      color: Theme.of(context).dividerColor,
                      style: BorderStyle.solid),
                  bottom: BorderSide(
                      color: Theme.of(context).dividerColor,
                      style: BorderStyle.solid)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return ItemPage();
                    }));
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.wallet_membership,
                        color: Colors.black,
                        size: 50,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: Text(
                          "Skin",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return DataScreen();
                    }));
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.shopping_bag_outlined,
                        color: Colors.blue,
                        size: 50,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: Text(
                          " Pemesanan Layanan",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
