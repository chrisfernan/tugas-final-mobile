import 'package:flutter/material.dart';
import 'package:purnamastore/auth/auth.dart';
import "./menu.dart" as beranda;
import './auth/auth1.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(new MaterialApp(
    title: "Penjualan Diamond Mobile Legend Termurah",
    home: new MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => Auth(),
      builder: (context, child) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Purnama Store",
        theme: ThemeData(
          primarySwatch: Colors.pink,
        ),
        home: LoginPage(),
      ),
    );
  }
}
